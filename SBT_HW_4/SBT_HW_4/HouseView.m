//
//  HouseView.m
//  SBT_HW_4
//
//  Created by Vovan on 22.04.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "HouseView.h"

@implementation HouseView

- (instancetype) init{
    self = [super init];
    if(self)
    {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    CGFloat beginWallsX = self.frame.size.width / 4;
    CGFloat beginWallsY = self.frame.size.height / 3;
    CGRect rectWalls = CGRectMake(beginWallsX, beginWallsY, self.frame.size.width - 2 * beginWallsX, self.frame.size.height - 2 * beginWallsY);
    [self drawWallsInRect:rectWalls];
    CGFloat offset = 20;
    CGFloat roofHeight = self.frame.size.height / 4;
    CGRect rectRoof = CGRectMake(beginWallsX - offset, beginWallsY - roofHeight, self.frame.size.width - 2 * beginWallsX + 2 * offset, roofHeight);
    [self drawRoofInRect:rectRoof];
    CGFloat offsetWX = rectWalls.size.width / 4;
    CGFloat offsetWY = rectWalls.size.height / 4;
    CGRect rectWindow = CGRectMake(rectWalls.origin.x + offsetWX, rectWalls.origin.y + offsetWY, rectWalls.size.width - 2 * offsetWX, rectWalls.size.height - 2 * offsetWY);
    [self drawWindowInRect:rectWindow];
    CGFloat offsetWRX = rectRoof.size.width / 2.8;
    CGFloat offsetWRY = rectRoof.size.height / 2.8;
    CGRect rectWindowRoof = CGRectMake(rectRoof.origin.x + offsetWRX, rectRoof.origin.y + offsetWRY, rectRoof.size.width - 2 * offsetWRX, rectRoof.size.height - 2 * offsetWRY);
    [self drawWindowRoofInRect:rectWindowRoof];
    CGRect rectTube = CGRectMake(rectRoof.origin.x + rectRoof.size.width / 5, rectRoof.origin.y + rectRoof.size.height / 4, rectRoof.size.width / 20, rectRoof.size.height / 3);
    [self drawTubeInRect:rectTube];
    
    CGFloat thickness = rectTube.size.width;
    for(int i = 0; i < 7; i++){
        CGRect rectBoard = CGRectMake(rectWalls.origin.x + rectWalls.size.width + i * thickness, rectWalls.origin.y + rectWalls.size.height / 3, thickness, 2 * rectWalls.size.height / 3);
        //CGRect rectBoard = rectTube;
        [self drawBoardInRect:rectBoard];
    }
    for(int i = 0; i < 7; i++){
        CGRect rectBoard = CGRectMake(rectWalls.origin.x - thickness - i * thickness, rectWalls.origin.y + rectWalls.size.height / 3, thickness, 2 * rectWalls.size.height / 3);
        //CGRect rectBoard = rectTube;
        [self drawBoardInRect:rectBoard];
    }
    
}

- (void) drawWallsInRect:(CGRect)rect{
    UIBezierPath *aPath = [UIBezierPath bezierPathWithRect:rect];
    [[UIColor redColor] setStroke];
    [aPath stroke];
}

- (void) drawRoofInRect:(CGRect)rect{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    [aPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)];
    [aPath closePath];
    [[UIColor blueColor] setStroke];
    [aPath stroke];
}

- (void) drawWindowInRect:(CGRect)rect{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    [aPath moveToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height)];
    [aPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height / 2)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height/ 2)];
    [[UIColor cyanColor] setStroke];
    [aPath stroke];
    UIBezierPath *bPath = [UIBezierPath bezierPathWithRect:rect];
    [bPath stroke];
}

- (void) drawWindowRoofInRect:(CGRect)rect{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    [aPath moveToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height)];
    [aPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height / 2)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height/ 2)];
    [[UIColor brownColor] setStroke];
    [aPath stroke];
    UIBezierPath *bPath = [UIBezierPath bezierPathWithOvalInRect:rect];
    [bPath stroke];
}

- (void) drawTubeInRect:(CGRect)rect{
    UIBezierPath *aPath = [UIBezierPath bezierPathWithRect:rect];
    [[UIColor orangeColor] setStroke];
    [aPath stroke];
}

- (void) drawBoardInRect:(CGRect)rect{
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    
    [aPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height / 5)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height / 5)];
    [aPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)];
    
    [[UIColor purpleColor] setStroke];
    [aPath closePath];
    [aPath stroke];
}

@end

















