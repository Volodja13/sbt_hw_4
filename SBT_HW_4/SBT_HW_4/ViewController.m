//
//  ViewController.m
//  SBT_HW_4
//
//  Created by Vovan on 22.04.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ViewController.h"
#import "HouseView.h"

@interface ViewController ()

@property(nonatomic, strong) HouseView *house;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    self.house = [HouseView new];
    [self.view addSubview:self.house];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
 
    // Dispose of any resources that can be recreated.
}

- (void) initView{
    CGFloat offset = 20;
    CGRect rect = CGRectMake(offset, offset, self.view.frame.size.width - 2 * offset,
                             self.view.frame.size.height - 2 * offset);
    self.house.frame = rect;
}

-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self initView];
    
    
}

@end
